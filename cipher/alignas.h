#pragma once

#include "compiler.h"
#include "language.h"

// Атрибут 'Alignas` может быть применен к объявлению переменной для указания требования к выравниванию.
 
#if LANGUAGE_IS_CXX11 || LANGUAGE_IS_CXX14 || LANGUAGE_IS_CXX17
     // Мы ничего не делаем, "alignas" - это ключевое слово

#elif LANGUAGE_IS_C11
     // Мы можем использовать `alignas`, определенный в <stdalign.h>.
     #include <stdalign.h>

#else
     /* Мы определяем "alignas` вручную. */
     #if !defined(_Alignas)
          #if COMPILER_IS_MSVC
               #define _Alignas(boundary) \
                    __declspec(align(boundary))

          #elif COMPILER_IS_GCC || COMPILER_IS_CLANG || COMPILER_IS_ICC
               #define _Alignas(boundary) \
                    __attribute__((__aligned__(boundary)))

          #endif
     #endif

     // Определяет удобный макрос `alignas`.
     #if !defined(alignas)
          #define alignas _Alignas
          #define __alignas_is_defined 1

     #endif
#endif
