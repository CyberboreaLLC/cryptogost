#define ALIGN(x) __attribute__((__aligned__(x)))

ALIGN(8) unsigned char testEncryptECB1BlockKey[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32};
ALIGN(8) unsigned char testEncryptECB1BlockInput[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
ALIGN(8) unsigned char testEncryptECB1BlockResult[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138};

ALIGN(8) unsigned char testEncryptECB2BlockKey[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32};
ALIGN(8) unsigned char testEncryptECB2BlockInput[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
ALIGN(8) unsigned char testEncryptECB2BlockResult[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138, 85, 86, 139, 228, 220, 54, 95, 208, 219, 15, 223, 60, 186, 116, 82, 23};

ALIGN(8) unsigned char testDecryptECB1BlockKey[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32};
ALIGN(8) unsigned char testDecryptECB1BlockInput[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138};
ALIGN(8) unsigned char testDecryptECB1BlockResult[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

ALIGN(8) unsigned char testDecryptECB2BlockKey[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32};
ALIGN(8) unsigned char testDecryptECB2BlockInput[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138, 85, 86, 139, 228, 220, 54, 95, 208, 219, 15, 223, 60, 186, 116, 82, 23};
ALIGN(8) unsigned char testDecryptECB2BlockResult[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};


ALIGN(8) unsigned char testEncryptCBC1BlockInitVector[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
ALIGN(8) unsigned char testEncryptCBC1BlockKey[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138, 85, 86, 139, 228, 220, 54, 95, 208, 219, 15, 223, 60, 186, 116, 82, 23};
ALIGN(8) unsigned char testEncryptCBC1BlockInput[] =  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
ALIGN(8) unsigned char testEncryptCBC1BlockResult[] =  {100, 167, 57, 201, 235, 241, 194, 182, 37, 132, 0, 220, 204, 97, 30, 122};


ALIGN(8) unsigned char testEncryptCBC2BlockInitVector[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
ALIGN(8) unsigned char testEncryptCBC2BlockKey[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138, 85, 86, 139, 228, 220, 54, 95, 208, 219, 15, 223, 60, 186, 116, 82, 23};
ALIGN(8) unsigned char testEncryptCBC2BlockInput[] =  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
ALIGN(8) unsigned char testEncryptCBC2BlockResult[] =  {100, 167, 57, 201, 235, 241, 194, 182, 37, 132, 0, 220, 204, 97, 30, 122, 156, 79, 24, 131, 16, 246, 139, 198, 16, 130, 133, 243, 44, 58, 155, 227};


ALIGN(8) unsigned char testDecryptCBC1BlockKey[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138, 85, 86, 139, 228, 220, 54, 95, 208, 219, 15, 223, 60, 186, 116, 82, 23};
ALIGN(8) unsigned char testDecryptCBC1BlockInput[] =  {145, 167, 113, 35, 168, 122, 103, 174, 27, 42, 62, 179, 52, 171, 123, 43, 100, 167, 57, 201, 235, 241, 194, 182, 37, 132, 0, 220, 204, 97, 30, 122};
ALIGN(8) unsigned char testDecryptCBC1BlockResult[] =  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

ALIGN(8) unsigned char testDecryptCBC2BlockKey[] = {202, 185, 56, 55, 49, 127, 63, 75, 55, 201, 24, 187, 155, 248, 187, 138, 85, 86, 139, 228, 220, 54, 95, 208, 219, 15, 223, 60, 186, 116, 82, 23};
ALIGN(8) unsigned char testDecryptCBC2BlockInput[] =  {145, 167, 113, 35, 168, 122, 103, 174, 27, 42, 62, 179, 52, 171, 123, 43, 100, 167, 57, 201, 235, 241, 194, 182, 37, 132, 0, 220, 204, 97, 30, 122, 156, 79, 24, 131, 16, 246, 139, 198, 16, 130, 133, 243, 44, 58, 155, 227};
ALIGN(8) unsigned char testDecryptCBC2BlockResult[] =  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};