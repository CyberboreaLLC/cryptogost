#include <iostream>
#include "streebog_test_data.h"
#include <gtest/gtest.h>

extern "C"
{
    unsigned char *GetHash(unsigned char *src, int32_t input_len, int32_t hash_len);
    void *memalloc(const size_t);
}

unsigned char *GetHashOfSequence(int input_len, int hash_size)
{
    unsigned char *str = (unsigned char *)memalloc(input_len);
    for (int i = 0; i < input_len; ++i)
    {
        str[i] = i % 256;
    }
    return GetHash(str, input_len, hash_size);
}


TEST(Streebogtest, M1_256)
{
    auto result = GetHash(testFromGostM1, sizeof(testFromGostM1), 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], resultFromGostM1_256[i]);
    }
}

TEST(Streebogtest, M1_512)
{
    auto result = GetHash(testFromGostM1, sizeof(testFromGostM1), 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], resultFromGostM1_512[i]);
    }
}

TEST(Streebogtest, M2_256)
{
    auto result = GetHash(testFromGostM2, sizeof(testFromGostM2), 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], resultFromGostM2_256[i]);
    }
}

TEST(Streebogtest, M2_512)
{
    auto result = GetHash(testFromGostM2, sizeof(testFromGostM2), 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], resultFromGostM2_512[i]);
    }
}

TEST(StreebogTest, String5Bytes_256)
{
    auto input_len = 5;
    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test5byteResult256[i]);
    }
}

TEST(StreebogTest, String5Bytes_512)
{
    auto input_len = 5;
    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test5byteResult512[i]);
    }
}

TEST(StreebogTest, String63Bytes_256)
{
    auto input_len = 63;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test63byteResult256[i]);
    }
}

TEST(StreebogTest, String63Bytes_512)
{
    auto input_len = 63;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test63byteResult512[i]);
    }
}

TEST(StreebogTest, String64Bytes_256)
{
    auto input_len = 64;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test64byteResult256[i]);
    }
}

TEST(StreebogTest, String64Bytes_512)
{
    auto input_len = 64;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test64byteResult512[i]);
    }
}

TEST(StreebogTest, String97Bytes_256)
{
    auto input_len = 97;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test97byteResult256[i]);
    }
}

TEST(StreebogTest, String97Bytes_512)
{
    auto input_len = 97;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test97byteResult512[i]);
    }
}

TEST(StreebogTest, String128Bytes_256)
{
    auto input_len = 128;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test128byteResult256[i]);
    }
}

TEST(StreebogTest, String128Bytes_512)
{
    auto input_len = 128;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test128byteResult512[i]);
    }
}

TEST(StreebogTest, String999Bytes_256)
{
    auto input_len = 999;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test999byteResult256[i]);
    }
}

TEST(StreebogTest, String999Bytes_512)
{
    auto input_len = 999;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test999byteResult512[i]);
    }
}

TEST(StreebogTest, String9999Bytes_256)
{
    auto input_len = 9999;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test9999byteResult256[i]);
    }
}

TEST(StreebogTest, String9999Bytes_512)
{
    auto input_len = 9999;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test9999byteResult512[i]);
    }
}

TEST(StreebogTest, String65536Bytes_256)
{
    auto input_len = 65536;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test65536byteResult256[i]);
    }
}

TEST(StreebogTest, String65536Bytes_512)
{
    auto input_len = 65536;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test65536byteResult512[i]);
    }
}

TEST(StreebogTest, String65537Bytes_256)
{
    auto input_len = 65537;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test65537byteResult256[i]);
    }
}

TEST(StreebogTest, String65537Bytes_512)
{
    auto input_len = 65537;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test65537byteResult512[i]);
    }
}

TEST(StreebogTest, String66666Bytes_256)
{
    auto input_len = 66666;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test66666byteResult256[i]);
    }
}

TEST(StreebogTest, String66666Bytes_512)
{
    auto input_len = 66666;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test66666byteResult512[i]);
    }
}

TEST(StreebogTest, String99999Bytes_256)
{
    auto input_len = 99999;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test99999byteResult256[i]);
    }
}

TEST(StreebogTest, String99999Bytes_512)
{
    auto input_len = 99999;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test99999byteResult512[i]);
    }
}

TEST(StreebogTest, String100000Bytes_256)
{
    auto input_len = 100000;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test100000byteResult256[i]);
    }
}

TEST(StreebogTest, String100000Bytes_512)
{
    auto input_len = 100000;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test100000byteResult512[i]);
    }
}

TEST(StreebogTest, String150000Bytes_256)
{
    auto input_len = 150000;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test150000byteResult256[i]);
    }
}

TEST(StreebogTest, String150000Bytes_512)
{
    auto input_len = 150000;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test150000byteResult512[i]);
    }
}

TEST(StreebogTest, String999999Bytes_256)
{
    auto input_len = 999999;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test999999byteResult256[i]);
    }
}

TEST(StreebogTest, String999999Bytes_512)
{
    auto input_len = 999999;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test999999byteResult512[i]);
    }
}

TEST(StreebogTest, String100000000Bytes_256)
{
    auto input_len = 100000000;

    auto result = GetHashOfSequence(input_len, 256);
    for (int i = 0; i < 32; ++i)
    {
        EXPECT_EQ(result[i], test100000000byteResult256[i]);
    }
}

TEST(StreebogTest, String100000000Bytes_512)
{
    auto input_len = 100000000;

    auto result = GetHashOfSequence(input_len, 512);
    for (int i = 0; i < 64; ++i)
    {
        EXPECT_EQ(result[i], test100000000byteResult512[i]);
    }
}