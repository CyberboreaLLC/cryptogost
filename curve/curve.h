#pragma once

#include <vector>
#include <string>
#include <random>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include "../cryptopp/ecp.h"

/// @brief фунция перевода хексовой строки в вектор байт
/// @param hex строка в шестнадцатеричном виде
/// @return вектор байт
std::vector<uint8_t> hex2bytes(const std::string &hex);

/// @brief оператор вывода вектора байт в поток
/// @param os поток вывода
/// @param v выводимый вектор
std::ostream &operator<<(std::ostream &os, const std::vector<uint8_t> &v);

/// @brief фунция перевода байтов в строку в шестнадцатеричном виде
/// @param v вектор байт
/// @return представление вектора в шестнадцатеричной строке
std::string toHex(const std::vector<uint8_t> &v);
std::string toString(const CryptoPP::Integer &a, int base = 10);

/// @brief структура содержащая параметры эллиптической кривой
struct Curve
{
    std::string name;
    CryptoPP::Integer P;
    CryptoPP::Integer Q;

    CryptoPP::Integer A;
    CryptoPP::Integer B;

    CryptoPP::Integer X;
    CryptoPP::Integer Y;

    CryptoPP::ECP ecp;
    CryptoPP::ECP::Point point;

    Curve(std::string name, CryptoPP::Integer p, CryptoPP::Integer q, CryptoPP::Integer a, CryptoPP::Integer b, CryptoPP::Integer x, CryptoPP::Integer y);

    int pointSize();

    CryptoPP::Integer pos(CryptoPP::Integer v);
};

/// @brief по сути точка на эллиптической кривой
class PublicKey
{
public:
    Curve curve; // кривая на которой определен ключ
    CryptoPP::ECP::Point point; // точка на кривой (по сути и есть кллюч)

    PublicKey(Curve curve, CryptoPP::ECP::Point key);

    /// @brief перевод в хексовое представление
    /// @return хексовое представления публичного ключа
    std::string toHexString();

    /// @brief создание публичного ключа из его хексового представления
    /// @param curve эллиптичекая кривая
    /// @param raw хексовое представление публичного ключа
    /// @return публичный ключ
    static PublicKey CreateFromHex(Curve curve, std::string raw)
    {
        CryptoPP::Integer X((raw.substr(0, curve.pointSize() * 2)+ "h").data());

        CryptoPP::Integer Y((raw.substr(curve.pointSize() * 2) + "h").data());
        
        return PublicKey(curve, CryptoPP::ECP::Point(X, Y));
    }

    /// @brief фунция проверки подписи
    /// @param digest хеш подписанного сообщения
    /// @param signature подпись 
    /// @return является ли подпись подлиной
    bool verifyDigest(std::vector<uint8_t> digest, std::vector<uint8_t> signature);
};

/// @brief по сути большое число
class PrivateKey
{
public:
    // кривая на которой определен ключ
    Curve curve;
    // 32-байтное число (ключ по сути)
    CryptoPP::Integer key;

    /// @brief перевод в хексовое представление
    /// @return хексовое представление приватного ключа
    std::string toHexString();

    PrivateKey(Curve curve, std::string raw);

    /// @brief генерация публичного ключа соотвествующего данному приватному
    /// @return публичный ключ
    PublicKey GenPublicKey();

    /// @brief создание подписи
    /// @param _digest хеш сообщения которое мы хотим подписать
    /// @return подпись
    std::vector<uint8_t> signDigest(std::vector<uint8_t> _digest);
};

Curve MyCurve();

/// @brief генерация общего ключа
/// @param curve элептическая кривая
/// @param prv приватный ключ
/// @param pub публичный ключ
/// @param ukm user key material (соль)
/// @return общий ключ
std::vector<uint8_t> Kek(Curve curve, PrivateKey prv, PublicKey pub, CryptoPP::Integer ukm);
