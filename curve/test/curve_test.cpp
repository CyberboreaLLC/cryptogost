#include <gtest/gtest.h>
#include "../curve.h"
#include "curve_test_data.h"


TEST(PrivateKey, createFromHex) {
    PrivateKey pk = PrivateKey(MyCurve(), PrivateKeyHex);
    EXPECT_EQ(toString(pk.key,16), keyHex);
}

TEST(PrivateKey, convertToHex) {
    PrivateKey pk = PrivateKey(MyCurve(), PrivateKeyHex);
    EXPECT_EQ(pk.toHexString(), PrivateKeyHex);
}

TEST(PrivateKey, genPublicKey) {
    PrivateKey pk = PrivateKey(MyCurve(), PrivateKeyHex);
    PublicKey pub = pk.GenPublicKey();
    EXPECT_EQ(toString(pub.point.x,16), TestGenPublicKeyXResult);
    EXPECT_EQ(toString(pub.point.y,16), TestGenPublicKeyYResult);
}

TEST(PublicKey, createFromHex) {
    PublicKey pub = PublicKey::CreateFromHex(MyCurve(), PublicKeyHex);
    EXPECT_EQ(toString(pub.point.x,16), TestCreateFromHexX);
    EXPECT_EQ(toString(pub.point.y,16), TestCreateFromHexY);
}

TEST(PublicKey, toHexString) {
    PublicKey pub = PublicKey::CreateFromHex(MyCurve(), PublicKeyHex);
    EXPECT_EQ(pub.toHexString(), PublicKeyHex);
}

TEST(PublicKey, verifyDigest) {
    PublicKey pub = PublicKey::CreateFromHex(MyCurve(), PublicKeyHex);
    EXPECT_TRUE(pub.verifyDigest(TestVerifySignDigestInput,TestVerifySignSignInput));
}

TEST(Keys, signAndVerify) {
    PrivateKey pk = PrivateKey(MyCurve(), PrivateKeyHex);
    PublicKey pub = pk.GenPublicKey();
    EXPECT_TRUE(pub.verifyDigest(TestVerifySignDigestInput ,pk.signDigest(TestVerifySignDigestInput)));
}

TEST(Keys, genCommonKey) {
    PrivateKey pk = PrivateKey(MyCurve(), PrivateKeyHex);
    PublicKey pub = PublicKey::CreateFromHex(MyCurve(), TestCommonPublicInput);
    CryptoPP::Integer ukm((TestCommonUkmSeed + "h").data());
    std::cout << Kek(MyCurve(), pk, pub, ukm) << std::endl;
    EXPECT_EQ(Kek(MyCurve(), pk, pub, ukm), TestCommonResult);
}