#pragma once

#include <string>
#include <vector>

std::string PrivateKeyHex = "141e9f9e9cc9ac22b1e323df2d4f2935762b3f455a50df27da9c98e071e4918d";
std::string keyHex = "141e9f9e9cc9ac22b1e323df2d4f2935762b3f455a50df27da9c98e071e4918d";

std::string TestGenPublicKeyXResult = "1865de8ef455321f5b3067a467578554242e2dfa2022ea08848b152ff2848c54";
std::string TestGenPublicKeyYResult = "46d5847e5a398000eba28b085d89e586810c55390f08dd10fe78df2e254fe4fd";

std::string PublicKeyHex = "1865de8ef455321f5b3067a467578554242e2dfa2022ea08848b152ff2848c5446d5847e5a398000eba28b085d89e586810c55390f08dd10fe78df2e254fe4fd";
std::string TestCreateFromHexX = "1865de8ef455321f5b3067a467578554242e2dfa2022ea08848b152ff2848c54";
std::string TestCreateFromHexY = "46d5847e5a398000eba28b085d89e586810c55390f08dd10fe78df2e254fe4fd";

static std::vector<uint8_t> TestVerifySignDigestInput{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63};
static std::vector<uint8_t> TestVerifySignSignInput{ 1, 65, 129, 73, 140, 79, 154, 61, 141, 143, 129, 112, 169, 75, 122, 180, 181, 137, 140, 83, 134, 141, 115, 170, 179, 51, 98, 53, 66, 221, 176, 93, 56, 84, 114, 124, 113, 3, 91, 120, 236, 251, 46, 66, 162, 6, 32, 94, 150, 216, 66, 101, 12, 193, 58, 115, 152, 146, 49, 111, 141, 183, 122, 100}; 


std::string TestCommonPublicInput = "f3872409690bee7f2d69e56adf8ea6f2be6e6539fd9ba33e5a537d94a7d29ac1321ab32fc78a0fa836ed56304485b2cc3a162add40b7ad5a06fe2727ee358542";
std::string TestCommonUkmSeed  = "1d80603c8544c727";
std::vector<uint8_t> TestCommonResult{71, 137, 195, 100, 66, 24, 81, 84, 44, 76, 232, 142, 2, 18, 199, 157, 226, 97, 49, 103, 126, 117, 11, 173, 134, 106, 152, 125, 7, 3, 158, 104, 18, 179, 127, 224, 60, 8, 214, 116, 217, 50, 60, 17, 209, 144, 18, 171, 49, 247, 254, 137, 76, 146, 175, 242, 114, 45, 237, 22, 228, 54, 252, 10};